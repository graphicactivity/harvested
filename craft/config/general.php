<?php

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'allowAutoUpdates' => 'false',
    ),

    'thomasbaker.local' => array(
        'devMode' => true,
        'allowAutoUpdates' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/thomasbaker.local/public/',
            'baseUrl'  => 'http://thomasbaker.local/',
        )
    ),

    'harvested.co.nz' => array(
        'devMode' => false,
        'allowAutoUpdates' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://harvested.co.nz/',
        )
    )
);
