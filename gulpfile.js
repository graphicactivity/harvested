// Require

var gulp    = require('gulp'),
    gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    uglify  = require('gulp-uglify'),
    rename  = require('gulp-rename');


gulp.task('sass', function(){
  return gulp.src(['assets/css/*.css', 'node_modules/aos/dist/aos.css'])
  .pipe(prefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(concat('site.min.css'))
  .pipe(gulp.dest('public/css'));
});

gulp.task('watch', function () {
  gulp.watch('assets/css/*.css', ['sass']);
  gulp.watch('assets/js/*.js', ['scripts']);
});

gulp.task('scripts', function() {
  gulp.src([
    'assets/js/*.js',
    'node_modules/aos/dist/aos.js' ])
  .pipe(gulp.dest('public/js'))
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('public/js'));
});
